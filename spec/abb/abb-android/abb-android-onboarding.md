New Onboarding – ABB Android
========================================================

After analysing a lot of the feedback we're getting in the Play Store reviews, we have identified a few specific problems that are causing a degraded experience for our users. These are the main problems we want to tackle: 

1. There are some misconceptions around what our product is and what it does. A large number of users believe that by installing the Adblock Browser (ABB) they will no longer see ads on the Youtube app for Android, and that's just one example. 
2. There is also a lack of understanding of our vision for Acceptable Ads. We're not doing a great job explaining what they are and why we think Acceptable Ads are important for a sustainable web experience.
3. We don't have any product usage data, only some basic user acquisition and retention data and the Play Store reviews. Play Store reviews are our most reliable source as to understand what problems lead to a painful experience to our users, however Play Store reviews is a data point that only comes very late in the lifecycle of a user. Once we spot trends in the reviews it's already too late, we have already lost those users affected by those problems.

This new onboarding version aims at solving the problems described above by doing a few things: 

1. Have a more clear explanation for new users regarding what our product is and what it does. Specifically, where do block or not block ads. 
2. Be better at communicating our vision of Acceptable Ads. And when a user disagrees with our vision let users more easily find their way to the settings where they can turn off Acceptable Ads.
3. Ask our users to opt-in into our product improvement program, through which we collect annonymized usage data to help us understand how are users using our product.

This document aims at describing how this new onboarding should look and work.

***

TABLE OF CONTENTS
-----------------
1. [Goals](#markdown-header-goals)
2. [Success Criteria](#markdown-header-success-criteria)
3. [Functional Requirments](#markdown-header-functional-requirements)
    1. [A: Basic requirements](#markdown-header-a-basic-requirements)
    2. [B: Onboarding page 1 of 3](#markdown-header-b-onboarding-page-1-of-3)
    3. [C: Onboarding page 2 of 3](#markdown-header-c-onboarding-page-2-of-3)
    4. [D: Onboarding page 3 of 3](#markdown-header-d-onboarding-page-3-of-3)
    5. [E: Ad blocking preferences panel](#markdown-header-e-ad-blocking-preferences-panel)
    6. [F: Data requirements](#markdown-header-f-data-requirements)


***

Goals
-----------------------

- Educate and inform users about our vision of Acceptable Ads. Let users turn it off if they don't want any ads.
- Inform and ask users to opt-in for our product improvements program
- Explain to users where will our product will be able to block ads versus where it won't be able to do so.

[Go back up](#markdown-header-table-of-contents)

Success criteria
-----------------------

- The number of negative reviews regarding misconceptions around Acceptable Ads should decrease.
- Users understand what Acceptable Ads are, and they understand how they can turn them off if they so wish to do so. We really believe in our vision, and so we want to be upfront, and we believe we can still keep the same low Acceptable Ads opt-out rate we currently have.
- Not more than 2 out of 10 people opt-out of Acceptable Ads.
- At least 1 out of 10 people that go through the onboarding opted in to our Product Improvements Program.
- The number of negative reviews regarding misconceptions around our product blocking ads on 3rd-party apps should decrese.

[Go back up](#markdown-header-table-of-contents)


Functional Requirements
-----------------------

### **A: Basic requirements**

**A1:** Every new user, once they open App for the 1st time, should go through the onboarding.

**A2:** The onboarding consists of 3 steps users have to go through. The three pages are specified in detail below:

 - B: Onboarding page 1 of 3
 - C: Onboarding page 2 of 3
 - D: Onboarding page 3 of 3
 
**A3:** Users have to go through each step and cannot skip to the end of the onboarding. 

**A4:** Also, users should not be able to skip the onboarding altogether, meaning, if the 
users is midway through the onboarding and closes the App, once they reopen the App, they should have to go through all the steps in the onboarding again. Ideally we could save which step the user was when they left, and once they reopen the app take them back to that same step. 

**A5:** On Android, if users hit the physical back button, they should be able to go back, but not to close the onboarding. 

**A6:** Once users complete the onboarding they should not have to go through it again, even after future updates.

[Go back up](#markdown-header-table-of-contents)

***

### **B: Onboarding page 1 of 3**

**Overview:** This is a purely informative page, the only action the user can do is to tap on the "Got it" button (final copy TBD).

**B1:** If user taps on the "Got it" button we should take the user to page 2 of the onboarding.

**B2:** If user hits the pyshical back button on an Android phone, we should close the application (as explained in A5).

![Splash Screen](../../../res/abb/abb-android/abb-android-onboarding/01.png)

[Go back up](#markdown-header-table-of-contents)

***

### **C: Onboarding page 2 of 3**

**Overview:** In this page the user can decide whether or not they want to participate in the Product Improvements Program, by sending annonymised usage and crash data to our Engineering team.

**C1:** The user can check or uncheck the "Yes, count me in" checkbox. 

**C2:** By default, the checkbox will be checked.

**C3:** If user taps on the "Next" button we should take the user to page 3 of the onboarding. We should also persist whether the user opted-in or opeted-out of the Product Improvement Program.

**C4:** If user hits the pyshical back button on an Android phone, we should take the user to page 1 of the onboarding.


![Splash Screen](../../../res/abb/abb-android/abb-android-onboarding/02.png) 
![Splash Screen](../../../res/abb/abb-android/abb-android-onboarding/02b.png)


[Go back up](#markdown-header-table-of-contents)

***

### **D: Onboarding page 3 of 3**

**D1:** If user taps on the "Got it" button we should take the user to the New Tab page.

**D2:** If user taps on the "Change my ad blocking settings" link we should open the adblocking preferences panel as per the screenshot below on section E. 

![Splash Screen](../../../res/abb/abb-android/abb-android-onboarding/03.png)

[Go back up](#markdown-header-table-of-contents)

***

### **E: Ad blocking preferences panel**

**Overview:** In this page the user can decide whether or not they want change their adblocking settings before finishing the onboarding.

**E1:** On the ad blocking preferences panel, the user can choose to turn ON or OFF the following settings:

 - Block All Ads
 - Allow Acceptable Ads

**E2:** By default the following setup should be observed in the toggles: 
 
  - Block All Ads is **ON**
  - Allow Acceptable Ads is **ON**

**E3:** Toggle states behavior and depoendencies:

- If the user turns **OFF** the "Block All ads" toggle, the "Allow Acceptable Ads" toggle should be turned **OFF** as well. 
- If the user turns **ON** the "Block All ads" toggle, the "Allow Acceptable Ads" toggle should be turned **ON** as well.
- If the user turns **OFF** the "Allow Acceptable Ads" toggle, the "Block All ads" toggle should remain in the same state. 
- - If the user turns **ON** the "Allow Acceptable Ads" toggle, the "Block All ads" toggle should remain in the same state. 

**E4:** If the user taps on "Agree and Finish" button (final copy tbd), the Ad blocking preferences panel should close and the user should be taken page 3 of the onboarding (that was already opened).
Any changes the user has made to her ad blocking preferences should be persisted. 

**E4:** If the user hits the physical back button on an Android phone, the Ad blocking preferences panel should close and the user should be taken page 3 of the onboarding (that was already opened). *No changes will be persisted.*

![Splash Screen](../../../res/abb/abb-android/abb-android-onboarding/03b.png)

[Go back up](#markdown-header-table-of-contents)

***

### **F: Data requirements**

**Overview:** The following requirements assumes we have usage tracking in place. What we're aiming with tracking usage on this feature is to understand whether users are completing the onboarding funnel, and how long is taking them to do so. 

Specifically we want to be able to answer the following questions: 

 - Q1: What percentage of users complete the onboarding? (completion rate = completed/started)
 - Q2: What percentage of users complete each individual step? (e.g. completion rate of step 1 = completed step 1 / started and so on for the other steps)
 - Q3: How much time it takes to complete the whole funnel? (average, percentiles)
 - Q4: How much time it takes to complete each individual step? (average, percentiles)
 - Q5: What % of users open the ad blocking settings page?
 - Q6: What % of users opted-in/out for Acceptable Ads?
 - Q6: What % of users opted-in/out for the product improvements program?


Observation: there are several ways to track events in order to be able the abovementioned answers later on. Usually most tracking platforms have some sort of limit on the number of total events, so instead of having for example 1 event per each page of the onboarding visited, e.g. EventVisitedOnboardingPage1, EventVisitedOnboardingPage2, etc, we can have a generic Visit event and log a property of such event that tells us which page user visited, e.g. Visit("OnboardingPage1"), Visit("OnboardingPage2"), etc. That could apply for click events as well, i.e., better to have a generic click event versus dozens/hundred of events with different names.

**F1:** When users start the onboarding, we should track the following events:

- "visit" event with property "OnboardingPage1"

**F2:** When user goes to Page 2 of the onboarding, we should track the following events:

- "visit" event with property "OnboardingPage2" (which we will use to determine whether users completed step 1)
- "timeToComplete" event with property "OnboardingPage1" and property time with the number of seconds that took user from beggining of onboarding until landing on page 2.

**F3:** When user goes to Page 3 of the onboarding, we should track the following events:

- "visit" event with property "OnboardingPage3"
- "timeToComplete" event with property "OnboardingPage2" and property time with the number of seconds that took user from end of Page 1 until now.

**F4:** When user goes to AdBlocking Settings panel during the onboarding, we should track the following events:

- "visit" event with property "OnboardingAdBlockSettingsPanel"

**F5:** When user finishes onboarding, we should track the following events.

- "timeToComplete" event with property "OnboardingPage3" and property time with the number of seconds that took user from end of Page 2 until now.
- "timeToComplete" event with property "OnboardingTotal" and property time with the number of seconds from start to finish.
- "visit" event with property "NewTab"
- we should store as user properties** the following:
 - "Acceptable Ads" toggle value
 - "Product Improvements Program" toggle value.
 - "OnboardingTotalTime" in seconds

** these properties will help us segment users to understand how is their retention cohort different based on those properties

[Go back up](#markdown-header-table-of-contents)

***

_**Adblock Browser™**, **Adblock Plus™**, and **Acceptable Ads™** are registered trademarks of [eyeo GmbH](https://eyeo.com)._
